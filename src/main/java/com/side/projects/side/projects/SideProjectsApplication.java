package com.side.projects.side.projects;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SideProjectsApplication {

	public static void main(String[] args) {
		SpringApplication.run(SideProjectsApplication.class, args);
	}

}
