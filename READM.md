## TODOS

- [X] create 'hello' controller
- [X] add jococo
- [ ] add to repo in gitlab
- [ ] add linter check
- [ ] add register
- [ ] add login
- [ ] add test for register
- [ ] add test for login
- [ ] add swagger
- [ ] add dev application properties
- [ ] add prod application properties
- [ ] add CI/CD